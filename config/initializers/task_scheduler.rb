
#scheduler = Rufus::Scheduler.start_new
scheduler = Rufus::Scheduler.new

scheduler.cron("00 08 * * *") do    #(minute, hour, day_of_month, month, day_of_week)
  Appointment.send_message
end

scheduler.in '30d' do
  # do something in 5 days
  Company.checkActive #company is active 1 year, then becoming inactive
  Appointment.removeAppointments  #remove all appointments that are 1 month old
end



