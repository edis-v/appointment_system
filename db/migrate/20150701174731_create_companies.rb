class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :owner
      t.string :address
      t.string :phone
      t.string :email
      t.string :website
      t.boolean :active
      t.string  :minTime
      t.string :maxTime
      t.string :hiddenDays
      t.timestamps
    end
  end
end
