class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :appointment_type_id
      t.column :start, 'timestamp with time zone'
      t.column :end, 'timestamp with time zone'
      t.string :phone
      t.string :email
      t.string :client
      t.string :code
      t.integer :status
      t.timestamps
    end
  end
end
