class CreateAuthTokens < ActiveRecord::Migration
  def up
    create_table :auth_tokens do |t|
      t.string :auth_token
      t.integer :user_id
      #t.timestamps
    end
  end

  def down
    drop_table :auth_tokens
  end
end
