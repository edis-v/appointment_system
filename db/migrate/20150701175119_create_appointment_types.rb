class CreateAppointmentTypes < ActiveRecord::Migration
  def change
    create_table :appointment_types do |t|
      t.string :name
      t.integer :company_id
      t.integer :duration

      t.timestamps
    end
  end
end
