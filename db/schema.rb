# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150706082016) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "appointment_types", force: true do |t|
    t.string   "name"
    t.integer  "company_id"
    t.integer  "duration"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "appointments", force: true do |t|
    t.integer  "appointment_type_id"
    t.datetime "start"
    t.datetime "end"
    t.string   "phone"
    t.string   "email"
    t.string   "client"
    t.string   "code"
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "auth_tokens", force: true do |t|
    t.string  "auth_token"
    t.integer "user_id"
  end

  create_table "companies", force: true do |t|
    t.string   "name"
    t.string   "owner"
    t.string   "address"
    t.string   "phone"
    t.string   "email"
    t.string   "website"
    t.boolean  "active"
    t.string   "minTime"
    t.string   "maxTime"
    t.string   "hiddenDays"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "phone"
    t.integer  "role",            default: 0
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
