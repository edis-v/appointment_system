namespace :db do
  desc 'Erase and fill database'
  task :populate => :environment do
    require 'populator'
    require 'faker'

    [Company, AppointmentType, User].each(&:delete_all)


    Company.populate 5 do |company|
      company.name = Populator.words(1..4)
      company.address = Faker::Address.street_address
      company.owner = Faker::Name.name.titleize
      company.phone = Faker::PhoneNumber.phone_number
      company.email = Faker::Internet.email
      company.website = Faker::Internet.url
      company.active = [true,false]
      company.created_at = Faker::Time.between(2.days.ago, Time.now)
    end

    Company.all.each do |company|
      AppointmentType.populate 5 do |type|
        type.company_id = company.id
        type.name = Populator.sentences(1)
        type.duration = [20,30,40,50,60,80,100,120]

      end
      #create employees for company
      User.populate 4 do |user|
        user.company_id = company.id
        user.first_name = Faker::Name.first_name
        user.last_name = Faker::Name.last_name
        user.email = Faker::Internet.email
        user.phone = Faker::PhoneNumber.cell_phone
        user.password_digest = Populator.words(1)
        user.role = 0
      end
      #create owner for company
      User.populate 1 do |user|
        user.company_id = company.id
        user.first_name = Faker::Name.first_name
        user.last_name = Faker::Name.last_name
        user.email = Faker::Internet.email
        user.phone = Faker::PhoneNumber.cell_phone
        user.password_digest = Populator.words(1)
        user.role = 1
      end
    end

    #create two rootAdmin
    User.populate 2 do |user|
      user.company_id = nil
      user.first_name = Faker::Name.first_name
      user.last_name = Faker::Name.last_name
      user.email = Faker::Internet.email
      user.phone = Faker::PhoneNumber.cell_phone
      user.password_digest = Populator.words(1)
      user.role = 2

    end
    puts 'All Done'
  end
end