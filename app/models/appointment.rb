class Appointment < ActiveRecord::Base

  belongs_to :appointment_type
  has_one :company, :through => :appointment_type

  validates_presence_of :appointment_type_id
  validates_presence_of :start
  validates_presence_of :client
  validates_presence_of :phone

  validate :end_time, :on => [:create]
  validate :past_time



  scope :by_user_id, lambda {|user_id| where(:user_id => user_id)}
  scope :by_appointment_type_id, lambda {|appointment_type_id| where(:appointment_type_id => appointment_type_id)}


  def self.send_message
      appointments = Appointment.all.each do |appointment|
        begin
        start = Date.parse(appointment.start.to_s)
        puts "Start #{start}"
        if start == Date.today
          user = User.where(:id => appointment.user_id).first
          type = AppointmentType.where(:id => appointment.appointment_type_id).first
          number_to_send_to = "+38761920654"  #user.phone.to_s
          twilio_sid = "AC9a3ff2df345618eb8cb7c35954855e77"
          twilio_token = "ac7819c78d0dd36a0e5009f382328f03"
          twilio_phone_number = "2015095874"

          @client = Twilio::REST::Client.new twilio_sid, twilio_token

          message = @client.messages.create(
              :from => "+1#{twilio_phone_number}",
              :to => number_to_send_to,
              :body => "Hello #{user.first_name}, we want to inform You that you have appointment #{type.name} at #{appointment.start.strftime("%H:%M")}h"
          )

        end
        rescue
          puts "Invalid phone number"
        end

      end


  end

  def self.send_code(phone,code)
    begin
    number_to_send_to = "#{phone}"
    twilio_sid = "AC9a3ff2df345618eb8cb7c35954855e77"
    twilio_token = "ac7819c78d0dd36a0e5009f382328f03"
    twilio_phone_number = "2015095874"

    @client = Twilio::REST::Client.new twilio_sid, twilio_token

    message = @client.messages.create(
        :from => "+1#{twilio_phone_number}",
        :to => "+#{number_to_send_to}",
        :body => "Your code is #{code}"
    )
    rescue
      puts "Invalid phone number"
    end

  end

  def self.send_notification(appointment,type)

    begin
    number_to_send_to = "#{appointment.phone}"
    twilio_sid = "AC9a3ff2df345618eb8cb7c35954855e77"
    twilio_token = "ac7819c78d0dd36a0e5009f382328f03"
    twilio_phone_number = "2015095874"

    @client = Twilio::REST::Client.new twilio_sid, twilio_token
    if type == 'update'
      message = @client.messages.create(
          :from => "+1#{twilio_phone_number}",
          :to => "+#{number_to_send_to}",
          :body => "Your appointment is moved on #{(appointment.start.to_time).strftime("%e/%B %H:%M")} h"
      )
    elsif type == 'destroy'
      message = @client.messages.create(
          :from => "+1#{twilio_phone_number}",
          :to => "+#{number_to_send_to}",
          :body => "#{appointment.client}, Your appointment is removed"
      )
    end
    rescue
      puts "Invalid phone number"
      end
  end

  def self.removeAppointments
      Appointment.all.each do |appointment|
        if appointment.start < DateTime.now.at_beginning_of_day
          puts "Removed appointment #{appointment.client}"
          appointment.destroy
        end
      end
  end


  def end_time
    if Appointment.where('start >= ? and start <= ? and appointment_type_id = ? and status = ?',self.start.to_time, self.end.to_time, self.appointment_type_id,1).count > 0
      errors.add(:end, "There's no enough free time for this appointment")
    end
  end

  def past_time
    if self.start < DateTime.now
      errors.add(:start, "You can't add appointment in the past")
    end
  end



end
