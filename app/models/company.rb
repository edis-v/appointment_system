class Company < ActiveRecord::Base
  has_many :appointment_types
  has_many :appointments, :through => :appointment_types
  has_many :users

  validates_presence_of :name



  def self.checkActive
    Company.all.each do |company|
      if company.created_at < Time.now - 1.years
        puts "Updating company #{company.name}"
        company.update_attribute(:active, 0)
      end
    end
  end

end
