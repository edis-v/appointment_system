class AppointmentType < ActiveRecord::Base
  belongs_to :company
  has_many :appointments

  validates_presence_of :name
  validates_presence_of :company_id
  validates_presence_of :duration



  scope :by_company, lambda {|company_id| where(:company_id => company_id)}



end
