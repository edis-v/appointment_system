class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new
    alias_action :create, :read, :update, :destroy, :to => :crud

    unless user.new_record?
      def allowed_appointments(l_user)
        ids = []
        l_user.company.appointment_types.each do |at|
          ids << at.id
        end
        ids.join(', ')
      end
    end


    if user.new_record?
      puts "User is guest"
      can :crud, User, :id => user.id
      can :read, Company, :active => true
      can :read, AppointmentType
      can [:read, :create], Appointment

    else
      if user.employee?
        puts "User is employee"
        can [:read, :update], User, :id => user.id

        can :read, Company, ["id = #{user.company_id} and active = true"] do |company|
          company.id == user.company_id
        end
        can :read, AppointmentType, ["company_id = #{user.company_id}"] do |at|
          at.company_id == user.company_id
        end
        can :crud, Appointment, ["appointment_type_id IN (#{allowed_appointments(user)}) and status = 1 and start >= '#{(DateTime.now.at_beginning_of_day).iso8601}'"] do |appointment|
          appointment.appointment_type.company_id == user.company_id
        end
      elsif user.owner?
        puts "User is owner"
        can :crud, User, :company_id => user.company_id

        can :crud, AppointmentType, ["company_id = #{user.company_id}"] do |at|
          at.company_id == user.company_id
        end

        can :crud, Appointment, ["appointment_type_id IN (#{allowed_appointments(user)}) and status = 1 and start >= '#{(DateTime.now.at_beginning_of_day).iso8601}'"] do |appointment|
          appointment.appointment_type.company_id == user.company_id
        end
        can :crud, Company, ["id = #{user.company_id} and active = true"] do |company|
          company.id == user.company_id
        end
      elsif user.rootAdmin?
        puts "User is root admin"
        can :crud, :all
      end
    end




    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
