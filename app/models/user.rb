class User < ActiveRecord::Base
  has_secure_password

  has_many :auth_tokens
  belongs_to :company

  validates_presence_of :first_name
  validates_presence_of :last_name
  validates_presence_of :password, :on => :create


  enum role: [:employee, :owner, :rootAdmin]
end
