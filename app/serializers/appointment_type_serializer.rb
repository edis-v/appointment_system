class AppointmentTypeSerializer < ActiveModel::Serializer
  attributes :id, :name, :duration
end
