class AuthTokenSerializer < ActiveModel::Serializer
  attributes :auth_token
  has_one :user
end
