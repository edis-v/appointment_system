class AppointmentSerializer < ActiveModel::Serializer
  attributes :id,:start, :end, :client, :phone, :email
  has_one :appointment_type
end
