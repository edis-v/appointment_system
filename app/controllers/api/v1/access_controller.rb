class Api::V1::AccessController < ApplicationController

  skip_before_action :restrict_access, only: [:create]
  skip_authorization_check

  def create
    user = User.find_by_email(params[:access][:email])
    if user && user.authenticate(params[:access][:password])
      @token_object = AuthToken.create(:auth_token => generate_new_token, :user_id => user.id)
      render json: @token_object, status: 200
      # , :include => {:user => {:except => [:password_digest, :created_at, :updated_at],
      #                          :include => {:company => {}},
      # }} ,
      #     :except => [:id, :user_id]
    else
      render json: {message: 'Bad Credentials'}, status: 401
    end
  end

  def destroy
    user = User.find(params[:id])
    if user
      token = AuthToken.find_by_user_id(user.id)
      if token.destroy
        render json: {message: 'Successfully Logout'}, status: 200
      else
        render json: token.errors, status:404
      end
    else
      render json: {message: user.errors}, status: 404
    end
  end

  private

  def generate_new_token
    SecureRandom.uuid.gsub(/\-/,'')
  end

  def access_params
    params.require(:access).permit(:email, :password)
  end

end
