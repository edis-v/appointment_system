class Api::V1::UserTypesController < ApplicationController

  def index
    @user_types = UserType.all
    render json: @user_types, status: 200
  end

  def show
    @user_type = UserType.find(params[:id])
    render json: @user_type, status: 200
  end

  def create
    @user_type = UserType.new(user_type_params)
    if @user_type.save
      render json: @user_type, status: 201
    else
      render json: @user_type.errors, status: 404
    end
  end

  def update
    @user_type = UserType.find {params[:id]}
    if @user_type.update_attributes(user_type_params)
      render json: @user_type, status: 200
    else
      render json: @user_type.errors, status: 404
    end
  end

  def destroy
    @user_type = UserType.find(params[:id])
    if @user_type.destroy
      render nothing: true, status: 200
    else
      render json:@user_type.errors, status: 404
    end
  end

  private

  def user_type_params
    params.require(:user_type).permit(:name)
  end

end
