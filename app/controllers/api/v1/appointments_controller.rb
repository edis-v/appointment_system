class Api::V1::AppointmentsController < ApplicationController

  before_action :set_end_time, only: [:create]
  skip_before_action :restrict_access, only: [:create, :index]
  load_and_authorize_resource
  skip_load_and_authorize_resource :only => :confirm_code
  before_action :set_status, :only => [:create]

  def index

    if !current_user
      @appointments = Company.find(params[:company_id]).appointments if params[:company_id].present?
      @appointments = @appointments.where('start >= ? and status = ?', DateTime.now.at_beginning_of_day.iso8601, 1)
      render json: @appointments, status: 200
    else
      render json: @appointments, status: 200
    end
  end

  def show
    render json: @appointment, status: 200
  end

  def create
    @appointment.status = 1 if current_user && (current_user.employee? || current_user.owner?)
    if @appointment.save
      render json: @appointment, status: 201
      Appointment.send_code(@appointment.phone, @appointment.code) if current_user.blank?
    else
      render json: @appointment.errors, status: 404
    end
  end

  def update
    if @appointment.update_attributes(appointment_params)
      render json: @appointment, status: 200
      Appointment.send_notification(@appointment,'update')
    else
      render json: @appointment.errors, status: 404
    end
  end

  def destroy
    if @appointment.destroy
      render nothing: true, status: 200
      Appointment.send_notification(@appointment,'destroy')
    else
      render json: @appointment.errors, status: 404
    end
  end

  def confirm_code
    @appointment = Appointment.where(:code => params[:appointment][:code]).first
    if @appointment.update_attribute(:status, 1)
      render json: @appointment, status: 200
    else
      render json: @appointment.errors, status:404
    end

  end

  private

  def set_end_time
    date = DateTime.iso8601(params[:appointment][:start])
    duration = AppointmentType.find(params[:appointment][:appointment_type_id]).duration
    date = date + duration.minutes
    params[:appointment][:end] = Time.parse(date.iso8601)
  end

  def set_appointment
    @appointment = Appointment.find(params[:id])
  end

  def appointment_params
    params.require(:appointment).permit(:appointment_type_id, :start, :end, :client, :phone, :email, :code, :status)
  end

  def set_status
    @appointment.status = 0
    value = ''
    5.times{value  << (65 + rand(25)).chr}
    @appointment.code = value
  end

end
