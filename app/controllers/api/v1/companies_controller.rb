class Api::V1::CompaniesController < ApplicationController

  load_and_authorize_resource

  def index
    render json: @companies
  end

  def show
      render json: @company, status: 200
  end

  def create
    if @company.save
      render json: @company, status: 201
    else
      render json: @company.errors, status: 404
    end
  end

  def update
    if @company.update_attributes(company_params)
      render json: @company, status: 200
    else
      render json: @company.errors, status: 404
    end
  end

  def destroy
    if @company.destroy
      render nothing: true, status: 200
    else
      render json: @company.errors, status: 404
    end
  end

  private

  def company_params
    params.require(:company).permit(:name, :owner, :address, :phone, :website, :email, :active, :minTime, :maxTime, :hiddenDays)
  end

end
