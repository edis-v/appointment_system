class Api::V1::SendTextsController < ApplicationController
  def index
  end

  def send_text_message
    #number_to_send_to = params[:number_to_send_to]
    number_to_send_to = "+38761920654"

    twilio_sid = "AC9a3ff2df345618eb8cb7c35954855e77"
    twilio_token = "ac7819c78d0dd36a0e5009f382328f03"
    twilio_phone_number = "2015095874"

    @client = Twilio::REST::Client.new twilio_sid, twilio_token

    message = @client.messages.create(
        :from => "+1#{twilio_phone_number}",
        :to => number_to_send_to,
        :body => "This is an message. It gets sent to #{number_to_send_to}."
    )
    render json: message.status
  end
end
