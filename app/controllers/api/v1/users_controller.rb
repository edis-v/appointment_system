class Api::V1::UsersController < ApplicationController

  #before_action :set_user, only: [:show, :update, :destroy]
  load_and_authorize_resource

  def index
    @users = @users.where(:company_id => params[:company_id]) if params[:company_id].present?
    render json: @users, status: 200
  end

  def show
    render json: @user, status: 200
  end

  def create
    #@user = User.new(user_params)
    if @user.save
      render json: @user, status: 201
    else
      render json: @user.errors, status: 404
    end
  end

  def update
    if @user.update_attributes(user_params)
      render json: @user, status: 200
    else
      render json: @user.errors, status: 404
    end
  end

  def destroy
    if @user.destroy
      render nothing: true, status: 200
    else
      render json: @user.errors, status: 404
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :phone, :role, :company_id)
  end



end
