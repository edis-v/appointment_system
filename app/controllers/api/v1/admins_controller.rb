class Api::V1::AdminsController < ApplicationController

  def create
    @admin = Admin.new(admin_params)
    if @admin.save
      render json: @admin, status: 201
    else
      render json: @admin.errors, status: 404
    end
  end

  def destroy
    @admin = Admin.find(params[:id])
    if @admin.destroy
      render nothing: true, status: 200
    else
      render json: @admin.errors, status: 404
    end
  end

  private

  def admin_params
    params.require(:admin).permit(:user_id, :company_id)
  end

end
