class Api::V1::AppointmentTypesController < ApplicationController

  #before_action :set_appointment_type, only: [:show, :update, :destroy]
  load_and_authorize_resource


  def index
    @appointment_types = @appointment_types.where(:company_id => params[:company_id]) if params[:company_id].present?
    render json: @appointment_types, status:200
  end

  def show
    render json: @appointment_type, status: 200
  end

  def create
    #@appointment_type = AppointmentType.new(appointment_type_params)
    if @appointment_type.save
      render json: @appointment_type, status: 201
    else
      render json: @appointment_type.errors, status: 404
    end
  end

  def update
    if @appointment_type.update_attributes(appointment_type_params)
      render json: @appointment_type, status: 200
    else
      render json: @appointment_type.errors, status: 404
    end
  end

  def destroy
    if @appointment_type.destroy
      render nothing: true, status: 200
    else
      render json: @appointment_type.errors, status: 404
    end
  end

  private

  def set_appointment_type
    @appointment_type = AppointmentType.find(params[:id])
  end

  def appointment_type_params
    params.require(:appointment_type).permit(:name, :company_id, :duration)
  end

end
